" ========================================
" Plugins handled by vim-plug
" ========================================

call plug#begin()
" Plug 'LeBarbu/vim-epitech'
"Plug 'git@git.bastn.fr:vim-epitech'
Plug 'junegunn/vim-plug'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/syntastic'
Plug 'ggreer/the_silver_searcher'
Plug 'nanotech/jellybeans.vim'
Plug 'airblade/vim-gitgutter'
Plug 'fatih/vim-go'
Plug 'tpope/vim-dispatch'
call plug#end()

" The Silver Searcher
if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
endif

" Syntasic conf
let g:syntastic_always_populate_loc_list = 1

let g:syntastic_asm_checkers = 'nasm'
let g:syntastic_asm_dialect = 'intel'
let g:syntastic_asm_cflags = '-f elf64'

let b:asmsyntax = "nasm"
let b:syntastic_c_cflags = "-W -Wall -Wextra -pedantic"

let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

let python_highlight_all=1

" Treat .S files as nasm
autocmd BufNewFile,BufRead *.S setfiletype nasm

" ========================================
" Display
" ========================================

" Tell vim we have a dark background
set background=dark

" Theme
colorscheme jellybeans

" Switch syntax highlighting on
syntax on

" Custom symbols for tabstops and EOLs
set listchars=tab:▸\ ,eol:¬,space:␣

" update window title
set title

" display cursor position
set ruler

" Show what command is being typed in Vim
set showcmd

" Highlight matches when searching
set incsearch

" Highlight search results
set hlsearch

" Highlight when more than 80 columns
highlight ColorColumn ctermbg=darkblue
call matchadd('ColorColumn', '\%81v', 100)

" Display long lines on multiples lines
set wrap

" Display a minimum of lines around cursor
set scrolloff=3

" display line number
" set number

" Show relative line number
set relativenumber

" set line number
set number

" ========================================
" Global settings.
" ========================================

" Use vim settings, rather than vi settings
set nocompatible

" Enable file type detection and do language-dependent indenting.
filetype plugin indent on

" Make backspace behave in a sane manner.
set backspace=indent,eol,start

" Ignore case when search
set ignorecase

" If a search contains an uppercase word
" re-enable case sensitive
set smartcase

" Use Silver Searcher instead of grep
set grepprg=ag

" Detect when a file is changed
set autoread

" Error bells
set noerrorbells

" Automatically set indent of new line
set autoindent
set smartindent

" Disable backup
" set nobackup
" set nowritebackup
" set noswapfile
set backupdir=/tmp
set directory=/tmp

" Make Vim more liberal about hidden buffers
set hidden

" Remove delay
set timeout timeoutlen=300 ttimeoutlen=10

" ========================================
" Mapping
" ========================================

" Disable arroy keys
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" This is my leader key
let mapleader = ","

" Shortcut to rapidly toggle `set list`
nmap <leader>l :set list!<CR>

" Quick ag search
nmap <leader>t :Ag 

" Enable Ctrl-s for saving file
" We have to disable Software Flow Control
" with 'stty -ixon' for example.
map <C-s> <esc>:w<CR>
imap <C-s> <esc>:w<CR>

" Remove extra whitespace
nmap <leader><space> :call Preserve("%s/\\s\\+$//e")<cr>

" Toogle paste mode
nmap <leader>p :set paste!<cr>

" Toggle syntastic
nmap <leader>s :SyntasticToggleMode<cr>

" Zoom / restore window
nmap <leader>z :ZoomToggle<CR>

" Insert a real tab (While using supertab...)
inoremap <leader><tab> <c-v><tab>

" bind K to grep word under cursor
nnoremap K :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>

" bind \ (backward slash) to grep shortcut
command -nargs=+ -complete=file -bar Ag silent! grep! <args>|cwindow|redraw!
nnoremap \ :Ag<SPACE>

" ========================================
" Functions
" ========================================

" Zoom / restore window
function! s:ZoomToggle() abort
    if exists('t:zoomed') && t:zoomed
        execute t:zoom_winrestcmd
        let t:zoomed = 0
    else
        let t:zoom_winrestcmd = winrestcmd()
        resize
        vertical resize
        let t:zoomed = 1
    endif
endfunction
command! ZoomToggle call s:ZoomToggle()

" Run a command buf save where we was first

function! Preserve(command)
  " Preparation: save last search, and cursor position.
  let _s=@/
  let l = line(".")
  let c = col(".")
  " Do the business:
  execute a:command
  " Clean up: restore previous search history, and cursor position
  let @/=_s
  call cursor(l, c)
endfunction

" ========================================
" Epitech
" ========================================

let g:epi_login = 'dhiver_b'
let g:epi_name = 'Bastien DHIVER'
let g:epi_mode_emacs = 1

